#!/usr/bin/env python

import rospy
from baxter_interface import Limb, RobotEnable, Head
from baxter_core_msgs.msg import EndpointState 
from geometry_msgs.msg import PointStamped
import numpy as np

"""
Class for gaze following baxter limb
Needs arm side as a string parameter
"""
class Follow():

    def __init__(self, arm, follow_arms=False):
        self.arm = Limb(arm)
        self.head = Head()
        self.baxter = RobotEnable()
        self.baxter.enable()
        self.follow_arms = follow_arms
        self.point = None
        self.pub = rospy.Publisher("gaze/coordinate_points", PointStamped, queue_size=5)
        rospy.Subscriber("/robot/limb/{}/endpoint_state".format(arm), EndpointState, self.arm_cb, queue_size=5)
        rospy.Subscriber("/transformed_point", PointStamped, self.trans_cb)

    def arm_cb(self, msg):
        point = PointStamped()
        point.header.frame_id = "world"
        arm_coords = msg.pose.position
        point.point.x = arm_coords.x
        point.point.y = arm_coords.y
        point.point.z = arm_coords.z
        # if self.follow_arms and any(abs(value) > 0.1 for value in self.arm.endpoint_velocity()["linear"]):
        #     self.pub.publish(point)

    def trans_cb(self, msg):
        self.point = msg.point

    def run(self, function, *args, **kwargs):
        self.enable()
        function(*args, **kwargs)
        self.disable()

    def disable(self):
        self.follow_arms = False
    
    def enable(self):
        self.follow_arms = True

    # Follow by turning monitor as well
    def pan(self):
        while not rospy.is_shutdown():
            rospy.sleep(1)
            if self.point:
                angle = -np.arctan2(-self.point.x, self.point.z) / 1.57
                self.head.set_pan(angle, speed=0.20)


if __name__ == "__main__":
    rospy.init_node("follow_arm")
    f = Follow("left")
    f.pan()
    rospy.spin()

