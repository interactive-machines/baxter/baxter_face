#!/usr/bin/env python

import cv_bridge
import cv2
import dlib
import threading
import time
import sys
import rospy
import baxter_interface
import rospkg
import os
from baxter_interface import CHECK_VERSION, Limb
from lab_baxter_common.camera_toolkit.camera_control_helpers import CameraController
from std_msgs.msg import Bool, Int32
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point, PointStamped
from baxter_core_msgs.msg import HeadPanCommand, EndpointState


rospack = rospkg.RosPack()
cascPath = os.path.join(rospack.get_path("baxter_general_toolkit"), 'python_src/lab_baxter_common/face_id/opencv-3.4.1/data/haarcascades/haarcascade_frontalface_default.xml')
faceCascade = cv2.CascadeClassifier(cascPath)
video_capture = cv2.VideoCapture(0)

OUTPUT_SIZE_WIDTH = 960
OUTPUT_SIZE_HEIGHT = 600

class CameraNode:
    def __init__(self):
        rospy.Subscriber('/kinect2/qhd/image_color_rect', Image, self._head_cb, queue_size=1)
        rospy.Subscriber("/robot/limb/left/endpoint_state", EndpointState, self._left_cb, queue_size=5)
        rospy.Subscriber("/robot/limb/right/endpoint_state", EndpointState, self._right_cb, queue_size=5)

        self._head_pub = rospy.Publisher('temporary', HeadPanCommand, queue_size=2)
        self._people_pub = rospy.Publisher('people', Int32, queue_size=2)
        self._pub = rospy.Publisher("gaze/pixels", Point, queue_size=2)
        self._gaze_pub = rospy.Publisher("gaze/coordinate_points", PointStamped, queue_size=5)
        self._people = Int32()
        self._msg = HeadPanCommand()
        self._left_follow_arms = False
        self._right_follow_arms = False
        self._last_image = None

        self._left = Limb("left")
        self._right = Limb("right")
        self._lpoint = None
        self._rpoint = None

        #The color of the rectangle we draw around the face
        self.rectangleColor = (0,165,255)

        #variables holding the current frame number and the current faceid
        self.frameCounter = 0
        self.currentFaceID = 0

        #Variables holding the correlation trackers and the name per faceid
        self.faceTrackers = {}
        self.faceNames = {}
        
    def _head_cb(self, msg):
        self._last_image = msg
    
    def _pause_cb(self, msg):
        self._last_pause = msg
    
    def _left_cb(self, msg):
        self._lpoint = PointStamped()
        self._lpoint.header.frame_id = "world"
        arm_coords = msg.pose.position
        self._lpoint.point.x = arm_coords.x
        self._lpoint.point.y = arm_coords.y
        self._lpoint.point.z = arm_coords.z
        
    def _right_cb(self, msg):
        self._rpoint = PointStamped()
        self._rpoint.header.frame_id = "world"
        arm_coords = msg.pose.position
        self._rpoint.point.x = arm_coords.x
        self._rpoint.point.y = arm_coords.y
        self._rpoint.point.z = arm_coords.z

    #We are not doing really face recognition
    def doRecognizePerson(self, faceNames, fid):
        time.sleep(1)
        faceNames[ fid ] = "Person " + str(fid)
        
    def detect(self):  
        head = baxter_interface.Head()
        try:
            while not rospy.is_shutdown():
                # Capture frame-by-frame
                if self._last_image != None:
                    if any(abs(value) > 0.05 for value in self._left.endpoint_velocity()["linear"]):
                        self._left_follow_arms = True
                    else:
                        self._left_follow_arms = False
                    
                    if any(abs(value) > 0.05 for value in self._right.endpoint_velocity()["linear"]):
                        self._right_follow_arms = True
                    else:
                        self._right_follow_arms = False
                
                    frame = cv_bridge.CvBridge().imgmsg_to_cv2(self._last_image, desired_encoding='bgr8')
                    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                    faces = faceCascade.detectMultiScale(
                        gray,
                        scaleFactor=1.5,
                        minNeighbors=5,
                        minSize=(20, 20),
                        flags=cv2.CASCADE_SCALE_IMAGE
                    )

                    resultImage = frame.copy()
                    self.frameCounter += 1 
                    
                    fidsToDelete = []
                    for fid in self.faceTrackers.keys():
                        trackingQuality = self.faceTrackers[ fid ].update( frame )

                        #If the tracking quality is not good enough, we must delete
                        #this tracker
                        if trackingQuality < 10:
                            fidsToDelete.append( fid )

                    for fid in fidsToDelete:
                        self.faceTrackers.pop( fid , None )
                                    
                    #Loop over all faces and check if the area for this
                    #face is the largest so far
                    #We need to convert it to int here because of the
                    #requirement of the dlib tracker. If we omit the cast to
                    #int here, you will get cast errors since the detector
                    #returns numpy.int32 and the tracker requires an int
                    for (_x,_y,_w,_h) in faces:
                        x = int(_x)
                        y = int(_y)
                        w = int(_w)
                        h = int(_h)

                        #calculate the centerpoint
                        x_bar = x + 0.5 * w
                        y_bar = y + 0.5 * h

                        #Variable holding information which faceid we 
                        #matched with
                        matchedFid = None

                        #Now loop over all the trackers and check if the 
                        #centerpoint of the face is within the box of a 
                        #tracker
                        for fid in self.faceTrackers.keys():
                            tracked_position =  self.faceTrackers[fid].get_position()

                            t_x = int(tracked_position.left())
                            t_y = int(tracked_position.top())
                            t_w = int(tracked_position.width())
                            t_h = int(tracked_position.height())


                            #calculate the centerpoint
                            t_x_bar = t_x + 0.5 * t_w
                            t_y_bar = t_y + 0.5 * t_h

                            #check if the centerpoint of the face is within the 
                            #rectangleof a tracker region. Also, the centerpoint
                            #of the tracker region must be within the region 
                            #detected as a face. If both of these conditions hold
                            #we have a match
                            if ( ( t_x <= x_bar   <= (t_x + t_w)) and 
                                 ( t_y <= y_bar   <= (t_y + t_h)) and 
                                 ( x   <= t_x_bar <= (x   + w  )) and 
                                 ( y   <= t_y_bar <= (y   + h  ))):
                                matchedFid = fid

                        #If no matched fid, then we have to create a new tracker
                        if matchedFid is None:

                            #Create and store the tracker 
                            tracker = dlib.correlation_tracker()
                            tracker.start_track(frame,
                                                dlib.rectangle( x-10,
                                                                y-20,
                                                                x+w+10,
                                                                y+h+20))

                            self.faceTrackers[ self.currentFaceID ] = tracker

                            #Start a new thread that is used to simulate 
                            #face recognition. This is not yet implemented in this
                            #version :)
                            t = threading.Thread( target = self.doRecognizePerson ,
                                                   args=(self.faceNames, self.currentFaceID))
                            t.start()

                            #Increase the currentFaceID counter
                            self.currentFaceID += 1

                        #Now loop over all the trackers we have and draw the rectangle
                        #around the detected faces. If we 'know' the name for this person
                        #(i.e. the recognition thread is finished), we print the name
                        #of the person, otherwise the message indicating we are detecting
                        #the name of the person
                        
                        for fid in self.faceTrackers.keys():
                            tracked_position =  self.faceTrackers[fid].get_position()
                            t_x = int(tracked_position.left())
                            t_y = int(tracked_position.top())
                            t_w = int(tracked_position.width())
                            t_h = int(tracked_position.height())
                            
                            cv2.rectangle(resultImage, (t_x, t_y),
                                                    (t_x + t_w , t_y + t_h),
                                                    self.rectangleColor ,2)


                            if fid in self.faceNames.keys():
                                cv2.putText(resultImage, self.faceNames[fid] , 
                                            (int(t_x + t_w/2), int(t_y)), 
                                            cv2.FONT_HERSHEY_SIMPLEX,
                                            0.5, (255, 255, 255), 2)
                            else:
                                cv2.putText(resultImage, "Detecting..." , 
                                            (int(t_x + t_w/2), int(t_y)), 
                                            cv2.FONT_HERSHEY_SIMPLEX,
                                            0.5, (255, 255, 255), 2)

                    # get pause signal as ros parameter (boolean)
                    # pause = rospy.get_param('pause')                    
                    length = len(self.faceTrackers)
                    self._people.data = length
                    self._people_pub.publish(self._people) 

                    # comment out three lines below if you want to only pause and run another script
                    # otherwise this ends the current call of detect so other functions can
                    # run within the same script
                    # if pause == True:
                    #     rospy.set_param('pause', 'false')
                    #     break
                                                       
                    if length != 0:
                        # calculate average of face positions
                        avg_x = 0
                        avg_y = 0
                        for x in self.faceTrackers.keys():
                            avg_x += self.faceTrackers[x].get_position().center().x
                            avg_y += self.faceTrackers[x].get_position().center().y
                        avg_x /= length
                        avg_y /= length

                        point = Point()
                        point.x = avg_x
                        point.y = avg_y

                        if self._left_follow_arms:
                            self._gaze_pub.publish(self._lpoint)
                        elif self._right_follow_arms:
                            self._gaze_pub.publish(self._rpoint)
                        else:
                            self._pub.publish(point)

                    cv2.imshow("Head Camera Video Feed", resultImage)

                # 'q' is escape key
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                
        except KeyboardInterrupt as e:
            pass
                
        # When everything is done, release the capture
        video_capture.release()
        cv2.destroyAllWindows()

if __name__ == "__main__":
    rospy.init_node("pan")
    c = CameraNode()
    c.detect()
	
