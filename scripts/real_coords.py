#!/usr/bin/env python

import cv2
import rospy
from sensor_msgs.msg import Image, PointCloud2, CameraInfo
from geometry_msgs.msg import Point, PointStamped
from cv_bridge import CvBridge, CvBridgeError

'''
Node for outputting real (x,y) coordinates of objects from kinect
'''
class RealCoords(object):

    # Initalize subscriber and image
    def __init__(self):
        self.image_sub = rospy.Subscriber("/kinect2/qhd/image_color_rect", Image, self.image_callback)
        self.depth_sub = rospy.Subscriber("/kinect2/qhd/image_depth_rect", Image, self.depth_callback)
        self.info_sub = rospy.Subscriber("/kinect2/qhd/camera_info", CameraInfo, self.info_callback)
        self.pixel_sub = rospy.Subscriber("gaze/pixels", Point, self.pixel_callback)
        self.pub = rospy.Publisher("gaze/coordinate_points", PointStamped, queue_size=1)
        self.bridge = CvBridge()
        self.last_x = None
        self.last_y = None
        self.x = None
        self.y = None
        self.z = 1.5
        self.image = None
        self.depth = None
        self.K = None

    # callback for rgb_image subscriber
    def image_callback(self, data):
        self.image = self.bridge.imgmsg_to_cv2(data, "bgr8")

    # callback for depth image subscriber
    def depth_callback(self, msg):
        self.depth = self.bridge.imgmsg_to_cv2(msg, msg.encoding)
        if self.x and self.y:
            self.z = self.depth[self.y][self.x] / 1000.0

    # callback for transformation matrix
    def info_callback(self, msg):
        self.K = msg.K 

    # callback for kinect face detection pixel subscriber
    def pixel_callback(self, msg):
        self.x = int(msg.x)
        self.y = int(msg.y)    

    # transform pixel to x,y and publish
    def output(self):
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            # calculate coordinates for that point and publish
            if self.K and self.x and self.y and self.z != 0.0:
                point = PointStamped()
                point.header.frame_id = "kinect2_rgb_optical_frame"
                point.point.x = (((self.x - self.K[2]) * self.z) / self.K[0])
                point.point.y = (((self.y - self.K[5]) * self.z) / self.K[4])
                point.point.z = self.z
                if self.x != self.last_x or self.y != self.last_y:
                    self.pub.publish(point)
            self.last_x = self.x
            self.last_y = self.y
            r.sleep()

 # Run node
if __name__ == "__main__":
    rospy.init_node("tracking")
    r = RealCoords()
    r.output()
    rospy.spin()
