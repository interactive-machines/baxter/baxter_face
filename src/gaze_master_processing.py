#!/usr/bin/env python
# sample node to move the eyes on the robot
import socket
from socket import error as socket_error
import rospy
from geometry_msgs.msg import PointStamped
import tf2_geometry_msgs
import tf2_ros

class GazeMaster:
    BUFFER_SIZE = 20  # Normally 1024, but we want fast response

    def __init__(self):

        # init node
        rospy.init_node('gaze_master', anonymous=False)

        # params
        self.TCP_IP = rospy.get_param("tcp_ip", '127.0.0.1')
        self.TCP_PORT = rospy.get_param("tcp_port", 5005)

        # internal vars
        self.conn = None
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        connected = False

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        # get static transforms from head to eyes (this way we only
        self.left_trans = None
        self.right_trans = None
        while self.left_trans is None and self.right_trans is None:
            try:
                self.left_trans = \
                    self.tfBuffer.lookup_transform("display", "world", rospy.Time(), rospy.Duration(1.0))
                self.right_trans = \
                    self.tfBuffer.lookup_transform("display", "world", rospy.Time(), rospy.Duration(1.0))

            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                rospy.logwarn("Waiting for left and right eye transforms from head_link")
                rospy.sleep(3)
        rospy.loginfo("Got eye static transforms")

        # subscribers
        rospy.Subscriber("gaze/coordinate_points", PointStamped, self.callback)

        while not connected:
            try:
                self.socket.bind((self.TCP_IP, self.TCP_PORT))
                connected = True
            except socket_error as serr:
                rospy.logerr(serr)
                rospy.sleep(10)

        self.socket.listen(1)

        self.conn, self.addr = self.socket.accept()
        print 'Connection address:', self.addr

        # do nothing.. just wait for gaze commands
        rate = rospy.Rate(20)  # Hz
        while not rospy.is_shutdown():
            rate.sleep()

        # be good with the environment
        self.close_bridge()


    def close_bridge(self):
        if self.conn is not None:
            self.conn.close()
        if self.socket is not None:
            self.socket.close()            


    def callback(self, point_msg):

        try:
            trans = \
                self.tfBuffer.lookup_transform("display", point_msg.header.frame_id, point_msg.header.stamp, rospy.Duration(0.1))

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.logerr("Failed to transform point in {} frame to head_camera".format(point_msg.header.frame_id))
            return

        head_point = tf2_geometry_msgs.do_transform_point(point_msg, trans)
        left_point = tf2_geometry_msgs.do_transform_point(head_point, self.left_trans)
        right_point = tf2_geometry_msgs.do_transform_point(head_point, self.right_trans)

        msg = "G %.2f %.2f %.2f %.2f %.2f %.2f\n" % (left_point.point.x, left_point.point.y, left_point.point.z,
                                                     right_point.point.x, right_point.point.y, right_point.point.z)
        rospy.loginfo(msg)
        try:
            if self.conn is not None:
                self.conn.send(msg)
        except:
            rospy.logwarn("Lost connection...")



if __name__ == '__main__':
    try:
        GazeMaster()
    except rospy.ROSInterruptException:
        pass
