
import rospy
from std_msgs.msg import String

# Follow by turning monitor as well
def set_emotion(emotion):
    pub = rospy.Publisher("gaze/emotion", String, queue_size=2)
    msg = String()
    msg.data = emotion
    pub.publish(msg)

if __name__ == "__main__":
    rospy.init_node("follow_arm")
    while not rospy.is_shutdown():
        set_emotion("sad")
        rospy.sleep(5)
        set_emotion("surprised")
        rospy.sleep(5)
        set_emotion("happy")
        rospy.sleep(5)
    rospy.spin()
