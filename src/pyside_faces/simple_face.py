#!/usr/bin/python
"""
Simple Shutter face with blinking option
"""

import sys
import numpy as np
import rospy
from std_msgs.msg import String
from baxter_face.msg import PupilsLocation
from PySide2 import QtWidgets
from PySide2.QtCore import QThread, SIGNAL
from pyside_face import PySideFace, move_app_to_shutter_screen
import pyside_face_params as fp
from sensor_msgs.msg import Image


class SimpleFaceNode(QThread):
    """Simple Face Node. Handles all ROS communication for the face GUI"""

    def __init__(self):

        # init thread
        QThread.__init__(self)

        # init ROS node
        rospy.init_node("SimpleFaceNode", anonymous=False)

        # params
        self.emotion = "happy"
        self.limit_pupil_position = rospy.get_param("~limit_pupils", True)
        self.blink = rospy.get_param("~blink", True)
        self.move_to_shutter_screen = rospy.get_param("~move_to_shutter_screen", True)

        # Subscribers
        rospy.Subscriber("gaze/pupils_location", PupilsLocation, self.gaze_callback)
        rospy.Subscriber("gaze/emotion", String, self.emotion_callback)
        self.pub = rospy.Publisher("/robot/xdisplay", Image, queue_size=10)

    def gaze_callback(self, gaze_msg):
        """
        Trigger change in GUI based on requested gaze direction
        :param gaze_msg: 2DGaze message
        """
        gaze_string = "{} {} {} {}".format(gaze_msg.left_eye.x, gaze_msg.left_eye.y,
                                           gaze_msg.right_eye.x, gaze_msg.right_eye.y)
        self.emit(SIGNAL('change_gaze(QString)'), gaze_string)

    def emotion_callback(self, msg):
        self.emotion = msg.data

class SimpleFace(PySideFace):
    """
    Simple face that blinks every once in a while.
    """

    def __init__(self):
        # initialize all ROS stuff
        self.node = SimpleFaceNode()
        # initialize the GUI
        super(SimpleFace, self).__init__()

    def change_gaze(self, gaze_string):
        """
        Process gaze request
        :param gaze_string: string with 2D gaze position for the left and right eye
        """
        tok = gaze_string.split()

        # Get the desired pupil position.
        # Note that we flip Y here because the drawing origin is in the top-left corner of the screen
        self.left_pupil_pos = np.array([float(tok[0]), -float(tok[1])])  # left pupil position
        self.right_pupil_pos = np.array([float(tok[2]), -float(tok[3])])  # right pupil position

        # print "before left", self.left_pupil_pos
        # print "before right", self.right_pupil_pos
        # limit the actual pupil position
        if self.node.limit_pupil_position:
            self.left_pupil_pos = self.limit_pupil(self.left_pupil_pos)
            self.right_pupil_pos = self.limit_pupil(self.right_pupil_pos)

        # print "after left", self.left_pupil_pos
        # print "after right", self.right_pupil_pos

        # print("Got new gaze: {} -> {} {}".format(gaze_string, self.left_pupil_pos, self.right_pupil_pos))

        self.update()

    def limit_pupil(self, pupil_position):
        """
        Limit the position of the pupil within an eye
        :param pupil_position: 2D pupil position within the eye
        :return: new pupil position
        """
        max_distance = (fp.EYE_DIAMETER - fp.PUPIL_DIAMETER) * 0.5
        distance = np.linalg.norm(pupil_position)
        if distance > max_distance:
            pupil_position = pupil_position * max_distance / distance
        return pupil_position

    def setup(self):
        super(SimpleFace, self).setup()

        # setup GUI based on ROS params
        self.enable_blinking = self.node.blink

    def setConnections(self):
        """Set connections"""
        super(SimpleFace, self).setConnections()

        # connect ROS signals with Qt
        self.connect(self.node, SIGNAL("change_gaze(QString)"), self.change_gaze)

    def draw(self, event, qpainter):
        super(SimpleFace, self).draw(event, qpainter, self.node.emotion)


def main():
    """
    Main function. Run the Qt application to render the face.
    """
    # create Qt application
    app = QtWidgets.QApplication(sys.argv)

    # create face widget
    face_gui = SimpleFace()

    # move widget to shutter screen
    if face_gui.node.move_to_shutter_screen:
        move_app_to_shutter_screen(app, face_gui)

    # run the app
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

