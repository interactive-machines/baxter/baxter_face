# Parameters for shutter's face

FACE_WIDTH = 1024                             # default width for face based on the native screen resolution
FACE_HEIGHT = 600                             # default height for face based on the native screen resolution

FACE_CENTER_X = FACE_WIDTH * 0.5              # horizontal center for the eyes (adjusted by 12px because the physical screen is not centered on the face)
FACE_CENTER_Y = FACE_HEIGHT * 0.4            # vertical center for the eyes

EYE_DIAMETER = FACE_WIDTH * 0.25              # size of the scleras of the eyes
PUPIL_DIAMETER = EYE_DIAMETER * 0.5           # size of the pupils

EYE_SEPARATION = EYE_DIAMETER * 0.6           # separation between the eyes

EYE_FX = 40                                   # default horizontal focal length for the eyes
EYE_FY = 40                                   # default vertical focal length for the eyes

MOUTH_OFFSET = 75                             # pixel offset for drawing mouth
MOUTH_START_ANGLE = -150 * 16                 # start angle for drawing mouth chord
MOUTH_SPAN_ANGLE = 120 * 16                   # angle for how much of a circle the chord spans

EYEBROW_START_ANGLE = 45 * 16                 # start angle for drawing eyebrow arc
EYEBROW_SPAN_ANGLE = 90 * 16                  # angle for how much of a circle the arc spans