"""
Base class for rendering faces for Shutter with PySide. Other faces should inherit PySideFace's structure.
"""
import numpy as np
from PySide2 import QtGui, QtCore, QtWidgets
import pyside_face_params as fp
import signal
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import rospy
import numpy as np

class PySideFace(QtWidgets.QWidget):

    def __init__(self):
        super(PySideFace, self).__init__()

        # Hack to close the interface with control-C on the command line
        # see https://stackoverflow.com/questions/5160577/ctrl-c-doesnt-work-with-pyqt
        signal.signal(signal.SIGINT, signal.SIG_DFL)

        # Qt setup
        self.setGeometry(0, 0, fp.FACE_WIDTH, fp.FACE_HEIGHT)  # x, y, w, h
        self.setWindowTitle('PySideFace')

        # general app parameters
        self.full_screen = False                        # enable fullscreen mode when the app starts?
        self.bg_color = (244, 4, 111)                 # background color

        # drawing parameters
        self.eye_stroke = 12                            # eye stroke width
        self.eye_stroke_color = (0, 0, 0)               # eye stroke color
        self.sclera_color = (200, 200, 200)             # sclera color
        self.pupil_color = (0, 0, 0)                    # pupil color

        # convenience variables for drawing the eyes
        self.left_eye_pos = np.array([fp.FACE_CENTER_X + fp.EYE_SEPARATION, fp.FACE_CENTER_Y])  # position of the left eye
        self.right_eye_pos = np.array([fp.FACE_CENTER_X - fp.EYE_SEPARATION, fp.FACE_CENTER_Y]) # position of the right eye
        self.left_pupil_pos = np.array([0, 0])          # left pupil position
        self.right_pupil_pos = np.array([0, 0])         # right pupil position
        self.pub = rospy.Publisher("/robot/xdisplay", Image, queue_size=10)
        self.bridge = CvBridge()

        # blinking parameters
        self.enable_blinking = True                     # enable blinking
        self.is_blinking = False                        # are the eyes blinking?
        self.blinking_timer = None                      # timer for next blink
        self.average_blink_wait = 5.2                   # average wait between blinks (in seconds)
        self.std_blink_wait = 3                         # std for the time between blinks (in seconds)
        self.min_blink_wait = 1                         # minimum time between blinks (in seconds)
        self.blink_duration = 0.3                       # duration of a blink (in seconds)

        self.setup()
        self.setConnections()

        # set fullscreen
        if self.full_screen:
            self.showFullScreen()

        # update blinking variables if blinking is enabled
        if self.enable_blinking:
            # blink once to start the timer...
            self.blinking_timeout()

        # show the window
        self.show()

    def setup(self):
        """Setup method. Prepare the interface and anything that is necessary for proper operation of the GUI"""
        pass

    def setConnections(self):
        """Set constant connections"""
        pass

    def setBackgroundColor(self, red, green, blue):
        """
        Set background color based on RGB values
        :param red: red color (over 255)
        :param green: green color (over 255)
        :param blue: blue color (over 255)
        """
        assert 0 <= red <= 255, "The red input to SetBackgroundColor() must be in [0,255]"
        assert 0 <= green <= 255, "The green input to SetBackgroundColor() must be in [0,255]"
        assert 0 <= blue <= 255, "The blue input to SetBackgroundColor() must be in [0,255]"
        self.bg_color = (red, green, blue)


    def setFullScreen(self, value):
        """
        Set fullscreen
        :param value: boolean indicating if full screen mode is desired
        """
        assert isinstance(value, bool), "The input value to setFullScreen() must be a boolean"
        self.full_screen = value

    def setBlink(self, value):
        """
        Set blinking mode
        :param value: boolean indicating if we want the eyes to blink every once in a while
        """
        assert isinstance(value, bool), "The input value to setBlink() must be a boolean"
        self.blink = value

    def paintEvent(self, event):
        """
        Paint window. Setup environment for rendering the face.
        :param event: event that triggered painting
        """
        self.image = QtGui.QImage(fp.FACE_WIDTH, fp.FACE_HEIGHT, QtGui.QImage.Format_RGB32) # image data
        self.image.fill(QtGui.QColor(self.bg_color[0], self.bg_color[1], self.bg_color[2]))
        qp = QtGui.QPainter(self.image)

        # HACK because the pixels are not square in shutter's screen
        # The physical dimensions of the screen are 154.08 x 85.92 mm (see https://cdn-shop.adafruit.com/product-files/2407/c3003.pdf)
        # And the native resolution of the screen is 800 x 480 pixels.
        # Given the physical dimensions, the height of the screen should be something like 446.11 pixels for 800 pixels wide.
        # Instead of asking for a weird screen resolution, here we scale the graphics through Qt.
        qp.scale(1.0, 1.07)

        # Request for smooth graphics
        qp.setRenderHint(QtGui.QPainter.Antialiasing)

        # Now we draw the content
        self.draw(event, qp)
        qp.end()

        width = self.image.width()
        height = self.image.height()

        ptr = self.image.constBits()
        arr = np.array(ptr).reshape(height, width, 4)  #  Copies the data

        self.pub.publish(self.bridge.cv2_to_imgmsg(arr, "8UC4"))

    def draw(self, event, qpainter, emotion):
        """
        Main draw function
        :param event: event that triggered the paint event
        :param qpainter: qpainter object to paint with
        """
        # draw the background
        p = self.palette()
        p.setColor(self.backgroundRole(), QtGui.QColor(self.bg_color[0], self.bg_color[1], self.bg_color[2]))
        self.setPalette(p)

        # render the eyes
        if self.is_blinking:
            self.drawBlink(qpainter, self.left_eye_pos)
            self.drawBlink(qpainter, self.right_eye_pos)

        else:
            self.drawEye(qpainter, self.left_pupil_pos, self.left_eye_pos)
            self.drawEye(qpainter, self.right_pupil_pos, self.right_eye_pos)

        self.drawMouth(qpainter, self.left_eye_pos, self.right_eye_pos, emotion)

    def blinking_timeout(self):
        """
        Start blinking!
        """
        if self.is_blinking:
            # if it was blinking before, then we disable blinking and start a new timer for the next blink
            self.is_blinking = False
            delay = np.random.normal(self.average_blink_wait, self.std_blink_wait)
            if delay < self.min_blink_wait:
                delay = self.min_blink_wait

        else:
            # if it was not blinking, we now blink for a short period of time
            self.is_blinking = True
            delay = self.blink_duration

        self.blinking_timer = QtCore.QTimer.singleShot(int(delay * 1000), self.blinking_timeout)
        self.update()

    def keyPressEvent(self, e):
        """
        Handle key press events
        :param e: event
        :note By default, this function closes the window if ESC is pressed.
        """
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def drawMouth(self, qpainter, left_eye_center, right_eye_center, emotion):
        """
        Draw mouth
        :param qpainter: QPainter object in which the eyes should be drawn over
        :param <side>_eye_center: 2D center of the eye in the screen coordinate frame (numpy array)
        """
        assert isinstance(left_eye_center, (np.ndarray, np.generic)), "The input eye_center should be a numpy array"
        assert isinstance(right_eye_center, (np.ndarray, np.generic)), "The input eye_center should be a numpy array"    

        # helper vars
        ewdiv2 = fp.EYE_DIAMETER * 0.5                 # half of the width of the eye
        epdiv2 = fp.PUPIL_DIAMETER * 0.5               # half of the width of the pupil

        # first, draw sclera of the eye
        color = QtGui.QColor(self.eye_stroke_color[0], self.eye_stroke_color[1], self.eye_stroke_color[2])
        pen = QtGui.QPen(color, self.eye_stroke)
        
        # emotion = "happy"
        # draw the mouth
        if emotion == "happy":
            qpainter.setPen(pen)
            qpainter.setBrush(QtGui.QColor(self.pupil_color[0], self.pupil_color[1], self.pupil_color[2]))
            qpainter.drawChord(right_eye_center[0], right_eye_center[1] - ewdiv2 + fp.MOUTH_OFFSET, left_eye_center[0]-right_eye_center[0], \
                           left_eye_center[0]-right_eye_center[0], fp.MOUTH_START_ANGLE, fp.MOUTH_SPAN_ANGLE)
        elif emotion == "sad":
            qpainter.setPen(pen)
            qpainter.setBrush(QtGui.QColor(self.pupil_color[0], self.pupil_color[1], self.pupil_color[2]))
            qpainter.drawArc(right_eye_center[0], right_eye_center[1] - ewdiv2 + fp.EYE_DIAMETER + 42, left_eye_center[0]-right_eye_center[0], \
                            left_eye_center[0]-right_eye_center[0], -fp.MOUTH_START_ANGLE * 0.92, -fp.MOUTH_SPAN_ANGLE * 0.8)
        elif emotion == "surprised":
            qpainter.setPen(pen)
            qpainter.setBrush(QtGui.QColor(self.pupil_color[0], self.pupil_color[1], self.pupil_color[2]))
            qpainter.drawChord(right_eye_center[0], right_eye_center[1] - ewdiv2 + fp.EYE_DIAMETER + 30, left_eye_center[0]-right_eye_center[0], \
                            left_eye_center[0]-right_eye_center[0], -fp.MOUTH_START_ANGLE, -fp.MOUTH_SPAN_ANGLE)

    def drawEye(self, qpainter, pupil_center, eye_center):
        """
        Draw eye
        :param qpainter: QPainter object in which the eyes should be drawn over
        :param pupil_center: 2D center of the pupil in the eye coordinate frame (numpy array)
        :param eye_center: 2D center of the eye in the screen coordinate frame (numpy array)
        """
        assert isinstance(pupil_center, (np.ndarray, np.generic)), "The input pupil_center should be a numpy array"
        assert isinstance(eye_center, (np.ndarray, np.generic)), "The input eye_center should be a numpy array"

        # helper vars
        ewdiv2 = fp.EYE_DIAMETER * 0.5                 # half of the width of the eye
        epdiv2 = fp.PUPIL_DIAMETER * 0.5               # half of the width of the pupil
        pupil_in_eye = pupil_center + eye_center       # position of the pupil in the eye

        # first, draw sclera of the eye
        color = QtGui.QColor(self.eye_stroke_color[0], self.eye_stroke_color[1], self.eye_stroke_color[2])
        pen = QtGui.QPen(color, self.eye_stroke)
        
        qpainter.setPen(pen)
        qpainter.setBrush(QtGui.QColor(self.sclera_color[0], self.sclera_color[1], self.sclera_color[2]))
        qpainter.drawEllipse(eye_center[0] - ewdiv2, eye_center[1] - ewdiv2, fp.EYE_DIAMETER, fp.EYE_DIAMETER)

        # draw the pupil
        qpainter.setPen(QtCore.Qt.NoPen)
        qpainter.setBrush(QtGui.QColor(self.pupil_color[0], self.pupil_color[1], self.pupil_color[2]))
        qpainter.drawEllipse(pupil_in_eye[0] - epdiv2, pupil_in_eye[1] - epdiv2, fp.PUPIL_DIAMETER, fp.PUPIL_DIAMETER)

        # draw the eyebrows
        qpainter.setPen(pen)
        qpainter.setBrush(QtGui.QColor(self.pupil_color[0], self.pupil_color[1], self.pupil_color[2]))
        qpainter.drawArc(eye_center[0] - ewdiv2, eye_center[1] - ewdiv2 - 45, fp.EYE_DIAMETER, fp.EYE_DIAMETER, fp.EYEBROW_START_ANGLE, fp.EYEBROW_SPAN_ANGLE)

    def drawBlink(self, qpainter, eye_center):
        """
        Draw blink
        :param qpainter: QPainter object in which the eyes should be drawn over
        :param eye_center: 2D center of the eye in the screen coordinate frame (numpy array)
        """
        assert isinstance(eye_center, (np.ndarray, np.generic)), "The input eye_center should be a numpy array"

        ewdiv2 = fp.EYE_DIAMETER*0.5              # half of the width of the eye

        # draw a simple line
        color = QtGui.QColor(self.eye_stroke_color[0], self.eye_stroke_color[1], self.eye_stroke_color[2])
        pen = QtGui.QPen(color, self.eye_stroke)
        qpainter.setPen(pen)

        qpainter.drawLine(eye_center[0] - ewdiv2, eye_center[1], eye_center[0] + ewdiv2, eye_center[1])


def find_shutter_screen_by_resolution(qapp):
    """
    Find which screen corresponds to shutter's face based on the screens' resolutions
    :param qapp: QApplication
    :return: screen index, QScreen
    """
    for i, s in enumerate(qapp.screens()):
        geo = s.geometry() # QRect
        if geo.width() == fp.FACE_WIDTH and geo.height() == fp.FACE_HEIGHT:
            return i, s
    return None, None


def move_app_to_shutter_screen(qapp, qwidget):
    """
    Move face widget to shutter screen. First, find the screen. Then move the widget and set full screen mode.
    :param qapp: QApplication
    :param qwidget: QWidget for shutter's face
    """
    pass
