// Example blinking face for JR2.0
/* Stages
 * 0 = neutral
 * 1 = surprised
 * 2 = sad
 * 3 = angry
 * 4 = bored
 * 5 = determined
 * 6 = happy
 * 7 = suspicious
 * 8 = thinking
 * 9 = happy 2
 * 10 = embarrassed
*/

import processing.net.*; 

float eye_width;
float pupil_size;
float eyes_sep;
float eyes_y;
float leye_x;
float reye_x;
float eyeRatio = 1;

// blink params
int next_blink_stamp = 0;
int average_blink_wait = 5200; // in millis
int variance_blink_wait = 9000; // in millis
int min_blink_wait = 1000; // in millis
int blink_start = 0;
int surprised_start = 0;
int surprised_wait = 600;
Boolean is_blinking = false;
float suspicious_count = 0.0;
float suspicious_frames = 60.0;
float suspicious_angle = 0;
int thinking_start = 0;
int thinking_wait = 500;
Boolean is_thinking = false;
int embarrassed_start = 0;
int embarrassed_wait = 1000;
int stage;

//overlays for the expressions
//PShape surprised_overlay;
PShape sad_overlay;
PShape angry_overlay;
PShape bored_overlay;
PShape determined_overlay;
PShape happy_overlay;
PShape happy_2_overlay;

// eyes central projection camera
float cam_fx;
float cam_fy;
float cam_cx;
float cam_cy;

// fixation point
float fixation_x = 0.0;
float fixation_y = 0.0;

float l_fixation_x = 0.0;
float l_fixation_y = 0.0;
float r_fixation_x = 0.0;
float r_fixation_y = 0.0;

float dx = 0.05; // displacement between eyes

// server
Client c; 
String input;
String data[]; 


void compute_next_blink() {
  int x = int(randomGaussian() * variance_blink_wait);
  next_blink_stamp = millis() + max(average_blink_wait + x, min_blink_wait);
}

void setup() {
  frameRate(30);
  fullScreen(2); // add screen number if we have multiple screens
  //size(800, 600);
  background(0);
  noStroke();

  // don't change
  stroke(0);
  strokeWeight(14);
  
  // hack because we couldn't get the resolution of the screen to show up properly
  width = int(width * 0.87);
  height = int(height * 0.87);
  print(width);

  eye_width = width*0.30;
  pupil_size = eye_width*0.5;
  eyes_sep = width/10.0;
  eyes_y = height*0.5;
  leye_x = (width - eyes_sep - eye_width)*0.5;
  reye_x = leye_x + eye_width + eyes_sep;

  compute_next_blink();

  // central camera projection
  cam_fx = 500.0; //width*0.5;
  cam_fy = cam_fx;
  cam_cx = width*0.5;
  cam_cy = height*0.5;

  //load overlays
  //surprised_overlay = loadShape("Surprised_Overlay.svg");
  sad_overlay = loadShape("Sad_Overlay.svg");
  angry_overlay = loadShape("Angry_Overlay.svg");
  bored_overlay = loadShape("Bored_Overlay.svg");
  determined_overlay = loadShape("Determined_Overlay.svg");
  happy_overlay = loadShape("Happy_Overlay.svg");
  happy_2_overlay = loadShape("Happy_Overlay_2.svg");
  
  stage = 0;
  
  // Connect to the server’s IP address and port­
  //c = new Client(this, "127.0.0.1", 5005); // Replace with your server’s IP and port
}

void drawEye(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize) {
  //choose size of the pupil
  if(stage == 6) pupilSize += 30;
  if(stage == 9) pupilSize += 15;
  // Calculate the position of the pupil
  PVector pupil = getPupilPosition(screen_x, screen_y, x, y, eyeSize, pupilSize);
  // Draw the eye and pupil
  strokeWeight(14);
  stroke(0);
  fill(255);
  if(stage == 1){
    ellipse(x, y, eyeSize, eyeSize * 1.3); //stretch eye if expression is surprised
  }else{
    ellipse(x, y, eyeSize, eyeSize * eyeRatio);
  }
  fill(0);
  ellipse(pupil.x, pupil.y, pupilSize, pupilSize * eyeRatio);
  if(stage == 9){ //add twinkle to eye if expression is happy 2
    fill(255);
    ellipse(pupil.x - pupilSize/4, pupil.y + pupilSize/4, pupilSize/4, pupilSize/4 * 1.4);
  }
}

PVector getPupilPosition(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize){
  if(stage == 7){ //move the eyes to the sides if expression is suspicious
    PVector pupil = new PVector((eyeSize - pupilSize)/2 * cos(suspicious_angle + suspicious_count * 2*PI/suspicious_frames), 0);
    pupil.add(x, y);
    suspicious_count++;
    if(suspicious_count >= suspicious_frames){
      stage = 0;
      suspicious_count = 0;
    }
    return pupil;
  }
  if(is_thinking){ //move the eyes up if expression is thinking
      if(millis() - thinking_start >= thinking_wait){
         is_thinking = false;
      }
      PVector pupil = new PVector(x, y).add(new PVector(0, -(eyeSize - pupilSize)/2));
      return pupil;
  }
  if(stage == 10){ //move eyes down if expression is embarrassed
      if(millis() - embarrassed_start >= embarrassed_wait){
         stage = 0;
      }
      PVector pupil = new PVector(x, y).add(new PVector(0, (eyeSize - pupilSize)/2));
      return pupil;
  }
  PVector pupil = new PVector(screen_x, screen_y);
  pupil.sub(x, y);
  pupil.limit((eyeSize - pupilSize) / 2);
  pupil.add(x, y);
  if(stage == 4){ //move pupil down if it is completely covered (expression is bored)
     while(pupil.y < y - pupilSize/4) pupil.add(new PVector(0, 5)); 
  }
  return pupil;
}

void drawSurprised(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize) {
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  //shapeMode(CENTER);
  //shape(surprised_overlay, x, y, eyeSize + 16, eyeSize * 1.2 + 20);
}

void drawSad(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize, boolean isLeft){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  if(isLeft) shape(sad_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
  else shape(sad_overlay, x, y, -eyeSize - 16, (eyeSize + 16) * eyeRatio);
}

void drawAngry(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize, boolean isLeft){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  if(isLeft) shape(angry_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
  else shape(angry_overlay, x, y, -eyeSize - 16, (eyeSize + 16) * eyeRatio);
}

void drawBored(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  shape(bored_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
}

void drawDetermined(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  shape(determined_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
}

void drawHappy(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize, boolean isLeft){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  if(isLeft) shape(happy_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
  else shape(happy_overlay, x, y, -eyeSize - 16, (eyeSize + 16) * eyeRatio);
}

void drawHappy2(float screen_x, float screen_y, float x, float y, float eyeSize, float pupilSize, boolean isLeft){
  drawEye(screen_x, screen_y, x, y, eyeSize, pupilSize);
  shapeMode(CENTER);
  //add overlay
  if(isLeft) shape(happy_2_overlay, x, y, eyeSize + 16, (eyeSize + 16) * eyeRatio);
  else shape(happy_2_overlay, x, y, -eyeSize - 16, (eyeSize + 16) * eyeRatio);
}

void drawBlink(float x, float y, float eyeSize) {
  strokeWeight(14);
  line(x-eyeSize*0.5, y, x+eyeSize*0.5, y);
}

void blink() {
  drawBlink(leye_x, eyes_y, eye_width); // Left eye
  drawBlink(leye_x + eye_width*0.5 + eyes_sep + eye_width*0.5, eyes_y, eye_width); // Right eye
}

void receive_data() {
  // Receive data from server
  if (c.available() > 0) { 
    input = c.readString(); 
    input = input.substring(0, input.indexOf("\n"));  // Only up to the newline
    //print(input);
    data = split(input, ' ');  // Split values into an array

    String mode = data[0];
    print("'" + mode + "'");

    if (mode.equals("F") == true) {
      float X = float(data[1]);
      float Y = float(data[2]);
      float Z = float(data[3]);
      Z = 5.0;
      float eps = 0.001;
      if (Z <= eps && Z >= 0.0) {
        Z = eps;
      } else if (Z >= -eps && Z < 0.0) {
        Z = -eps;
      }

      //fixation_x = (X*cam_fx + cam_cx*Z)/Z;
      //fixation_y = (Y*cam_fy + cam_cy*Z)/Z;
      //print("Fixation "+fixation_x+ " " + fixation_y);

      l_fixation_x = leye_x - ((X+dx)*cam_fx)/Z;
      l_fixation_y =  eyes_y + (Y*cam_fy)/(2 * Z);

      r_fixation_x = reye_x - ((X-dx)*cam_fx)/Z;
      r_fixation_y = eyes_y + (Y*cam_fy)/(2 * Z);

      //print("Fixation "+l_fixation_x+ " " + l_fixation_y);
    }
  }
}

void draw() {
  //receive_data();

  // uncomment for using mouse input
  float look_x = mouseX;
  float look_y = mouseY;

  //float look_x = fixation_x;
  //float look_y = fixation_y;

  if(stage == 10){
    color c1 = color(100, 100, 100);
    color c2 = color(239, 80, 152);
    for(int y = 0; y < height; y++) {
      float n = map(y, 0, height * 3, 0, 1);
      color newc = lerpColor(c1, c2, n);
      stroke(newc);
      line(0, y, width, y);
    }
  }else{
    background(100);
  }
  if (is_blinking) {
    if (millis() - blink_start > 300) {
      is_blinking = false;
    } else {
      blink();
    }
  } else {
    if (next_blink_stamp < millis() /*|| mousePressed == true */) {
      // blink
      blink();
      compute_next_blink();
      blink_start = millis();
      is_blinking = true;
    } else {
      // normal eyes
      if(stage == 1 && millis() - surprised_start >= surprised_wait) stage = 0;
      switch(stage) {
        case 0: //neutral
          drawEye(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawEye(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;
        case 1: 
          drawSurprised(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawSurprised(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;  
        case 2:
          drawSad(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size, true); // Left eye
          drawSad(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size, false); // Right eye
          break;
        case 3: 
          drawAngry(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size, true); // Left eye
          drawAngry(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size, false); // Right eye
          break;
        case 4:
          drawBored(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawBored(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;
        case 5: 
          drawDetermined(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawDetermined(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;
        case 6:
          drawHappy(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size, true); // Left eye
          drawHappy(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size, false); // Right eye
          break;
        case 7: //suspicious
          drawDetermined(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawDetermined(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;
        case 9:
          drawHappy2(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size, true); // Left eye
          drawHappy2(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size, false); // Right eye
          break;
        case 10: //embarrassed
          drawEye(look_x, look_y, leye_x, eyes_y, eye_width, pupil_size); // Left eye
          drawEye(look_x, look_y, reye_x, eyes_y, eye_width, pupil_size); // Right eye
          break;
    }
    }
  }
  // draw fixation for debugging
  //strokeWeight(0);
  //fill(255, 0, 0);
  //ellipse(l_fixation_x, l_fixation_y, 10, 10);
  //fill(0, 255, 0);
  //ellipse(r_fixation_x, r_fixation_y, 10, 10);
}

void keyPressed(){
   if(key == '0') stage = 0;
   if(key == '1'){
     stage = 1;
     surprised_start = millis();
   }
   if(key == '2') stage = 2;
   if(key == '3') stage = 3;
   if(key == '4') stage = 4;
   if(key == '5') stage = 5;
   if(key == '6') stage = 6;
   if(key == '7') stage = 7;
   if(key == '8'){
     is_thinking = true;
     thinking_start = millis();
   }
   if(key == '9') stage = 9;
   if(key == 'a'){
     stage = 10;
     embarrassed_start = millis();
   }
}

void stop() {
  c.stop();
} 
