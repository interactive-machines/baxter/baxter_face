import java.net.ConnectException;
import processing.net.*; 

// center of the screen face
float center_x;
float center_y;

// eye dimensions
float pupil_size;
float eye_width;

// eye model
float f_x = 40;
float f_y = 40;

// target 3D location
float Xl, Yl, Zl; // left eye
float Xr, Yr, Zr; // right eye

// separapation among the center of the eyes in pixels
float separation; 

// ROS client (for receiving new gaze commands)
Client c; 
String input;
String data[]; 

void setup() {
  fullScreen(2); // add screen number if we have multiple screens
  //size(800, 480);
  println("Dimensions:", width, height);
  
  frameRate(30);
  background(0);
  noStroke();
  
  center_x = width/2 + 12; // adjustment of 12 pixels because the screen is not centered on the face.
  center_y = height/2;
  eye_width = width*.3;
  pupil_size = eye_width*.5;
  
  Xl = 0;
  Yl = 0;
  Zl = -1.0;
  
  Xr = 0.0;
  Yr = 0.0;
  Zr = -1.0;

  separation = eye_width*0.6; //width/5;
    
  // don't change
  stroke(0);
  strokeWeight(14);
  
  while (true) {
    c = new Client(this, "127.0.0.1", 5005);
    if (c.active()) {
        break;
    }
    // sleep for a bit and retry..
    println("Waiting 2 seconds and retrying again...");
    delay(2000);
  }
  
  println("Connected to ROS.");
}

void drawEye(PVector pupil, float newCenter) {
  // Calculate the position of the pupil
  pupil.add(newCenter, center_y);

  // Draw the eye and pupil
  strokeWeight(12);
  stroke(0);
  fill(200);
  ellipse(newCenter, center_y, eye_width, eye_width);
  fill(0);
  ellipse(pupil.x, pupil.y, pupil_size, pupil_size);
}

void draw(){
   receive_data();
   background(100);
   drawEye(pupilPosition(Xr, Yr, Zr), center_x - separation);
   drawEye(pupilPosition(Xl, Yl, Zl), center_x + separation);
}

PVector pupilPosition(float person_x, float person_y, float person_z){
  PVector v = new PVector(f_x * person_x, f_y * person_y);
  v.div(person_z);
  v.limit((eye_width - pupil_size) / 2);
  return v;
}

void receive_data() {
  // Receive data from server
  if (c.available() > 0) { 
    input = c.readString(); 
    input = input.substring(0, input.indexOf("\n"));  // Only up to the newline
    data = split(input, ' ');  // Split values into an array

    String mode = data[0];
    //print("'" + mode + "'");

    if (mode.equals("G") == true) {
      // we flip y because the coordinate frame of processing has y going down
      // see https://processing.org/tutorials/drawing/
      Xl = float(data[1]);
      Yl = -float(data[2]);
      Zl = float(data[3]);
      Xr = float(data[4]);
      Yr = -float(data[5]);
      Zr = float(data[6]);
      print("Xl: " + Xl + " Yl: " + Yl + " Zl: " + Zl + "\n");
      print("Xr: " + Xr + " Yr: " + Yr + " Zr: " + Zr + "\n");
    }
  }
}

void keyPressed(){
   if(key == 'q') f_x += 1;
   if(key == 'a') f_x -= 1;
   if(key == 'w') f_y += 1;
   if(key == 's') f_y -= 1;
   print("fx: " + f_x + " fy:" + f_y + "\n");
}

/** Clean up right before closing script **/
void stop() {
  if (c.active()) {
    c.stop();
  }
}
