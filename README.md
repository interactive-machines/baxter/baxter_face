# Baxter Face Package

Code to render faces for the robot on its screen. 

## Requirements

The Qt version of Baxter's face requires the [PySide2](https://pypi.org/project/PySide2/) library.
You can easily install it by running `pip` within the baxter_face package:

```bash
$ roscd baxter_face
$ pip install -r requirements.txt --user
```

If you don't have pip installed, follow [these instructions](https://linuxconfig.org/how-to-install-pip-on-ubuntu-18-04-bionic-beaver) to install it.


## Quick Start

### Test simple face (Qt version) using ROS

Run the following launch file to start the face:

```bash
$ roslaunch baxter_face simple_face.launch
```

Then, send requests for changing the gaze direction of the robot through the `/gaze/coordinate_points` topic. For example:

```bash
$ rostopic pub /gaze/coordinate_points geometry_msgs/PointStamped "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: 'world'
point:
  x: 0.0
  y: 0.0
  z: 1.0" 
```


### Other faces

The initial versions of the face were made in processing and remain available in this repository
for historic reasons. 

##### Test simple face with mouse control

Run the processing/sketch_gaze_mouse/sketch_gaze_mouse.pde script. Edit the <display> number in the fullScreen(<display>) function (l. 84) if necessary.

##### Test simple face with ROS control

Run the "Gaze Master" node first:

```bash
$ rosrun baxter_face gaze_master_processing.py
```

Then, run the processing/sketch_gaze_ros/sketch_gaze_ros.pde script. As before, edit the <display> number in the fullScreen(<display>) function (l. 21) if necessary.

