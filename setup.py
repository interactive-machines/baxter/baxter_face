## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
d = generate_distutils_setup()
d["packages"] = ['pyside_faces']
d["package_dir"] = {'': 'src'}

setup(**d)